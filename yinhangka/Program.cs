﻿using System;

namespace CreditCard
{
    class Dele_publisher
    {
        
        public delegate void Delegate_fun(User uesr);
        public event Delegate_fun Warn; 
        public event Delegate_fun DeMoney;
        
        public void WarnTest(User user)
        {
            if (Warn != null)
            {
                Warn(user);
            }
            else
            {
                Console.WriteLine("还款日未到");
            }
        }
        public void DeMoneyTest(User user)
        {
            if (DeMoney != null)
            {
                DeMoney(user);
            }
            else
            {
                Console.WriteLine("还款日未到");
            }
        }
    }

    class Bank
    {
        public void Woring(User user)
        {
            Console.WriteLine("{0},还款日即将到来，请你准备还款，本期欠款{1}元！", user.Name, user.Debt);
            Console.WriteLine();

        }
        public void DeMoneying(User user)
        {
            Console.WriteLine("{0},您的借记卡余额为{1}元，本次还清信用卡{2}元，借记卡目前余额为{3}元！",
                 user.Name, user.Balance, user.Debt, (user.Balance - user.Debt));
            Console.WriteLine();
        }

    }
    class User
    {
        private int balance;
        private int debt;
        private string name;
        private int date;


        public User()
        {
        }

        public User(int balance, int debt, string name, int date)
        {
            this.balance = balance;
            this.debt = debt;
            this.name = name;
            this.date = date;
        }

        public int Balance { get => balance; set => balance = value; }
        public string Name { get => name; set => name = value; }
        public int Debt { get => debt; set => debt = value; }
        public int Date { get => date; set => date = value; }
    }

    class Dele_subscriber
    {
        static void Main(string[] agrs)
        {
            Dele_publisher d1 = new Dele_publisher();
            Dele_publisher d2 = new Dele_publisher();
            User user = new User(6000, 4999, "小明", 8000);
            User user1 = new User(8900, 5900, "小红", 6000);
            Bank bank = new Bank();
            d1.Warn += bank.Woring;
            d2.DeMoney += bank.DeMoneying;

            //模拟时间
            for (int i = 0; i <= 10000; i++)
            {
                if (user1.Date - i == 1000)
                    d1.WarnTest(user1);

                if (user.Date - i == 1000)
                    d1.WarnTest(user);

                if (i == user1.Date)
                    d2.DeMoneyTest(user1);

                if (i == user.Date)
                    d2.DeMoneyTest(user);
            }
        }
    }
}